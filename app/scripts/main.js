// Swiper

    var swiper = new Swiper('.swiper-container', {
      slidesPerView: 1,
      spaceBetween: 30,
      loop: true,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });

    // Sticky header
    $(window).scroll(function(){
    if ($(window).scrollTop() >= 300) {
       $('nav').addClass('sticky');
    }
    else {
       $('nav').removeClass('sticky');
    }
});

$('.navbar-nav li').on('click', function(){
  $(this).find('.dropdown-menu').toggleClass('showed');
})
  
// toogle nav on mobile
// $(document).ready(function(){
//     $(".navbar-toggle").click(function(){
//         $(".collapse").collapse('toggle');
//     });
    
// });

$(document).ready(function(){
    $(".search").click(function(){
        $(".search-open").toggle();
    });
});